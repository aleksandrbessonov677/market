<?php

use yii\db\Migration;

/**
 * Class m230510_183036_message_history
 */
class m230510_183036_message_history extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('message_history', [
            'id' => $this->primaryKey(),
            'message' => $this->text(),
            'client_id' => $this->integer(),
            'created_at' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('message_history');
    }
}
