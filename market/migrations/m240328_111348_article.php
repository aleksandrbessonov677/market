<?php

use yii\db\Migration;

/**
 * Class m240328_111348_article
 */
class m240328_111348_article extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('article', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'article_category_id' => $this->integer(),
            'text' => $this->text(),
            'preview' => $this->string(),
            'view_count' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('article');
    }
}
