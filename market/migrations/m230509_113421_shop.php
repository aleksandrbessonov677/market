<?php

use yii\db\Migration;

/**
 * Class m230509_113421_shop
 */
class m230509_113421_shop extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('shop', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'rating' => $this->float(),
            'address' => $this->string(),
            'site' => $this->string(),
            'email' => $this->string(),
            'phone' => $this->string(),
            'user_id' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('shop');
    }
}
