<?php

use app\models\User;
use yii\db\Migration;

/**
 * Class m230505_162146_user
 */
class m230505_162146_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(),
            'last_name' => $this->string(),
            'phone' => $this->string(),
            'email' => $this->string()->unique(),
            'preview' => $this->string(),
            'role' => $this->string()->defaultValue('user'),
            'password' => $this->string(),
            'auth_key' => $this->string(),
            'access_token' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $user = new User();
        $user->email = Yii::$app->params['adminEmail'];
        $user->password = Yii::$app->params['adminPassword'];
        $user->role = User::ROLE_ADMIN;
        $user->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
