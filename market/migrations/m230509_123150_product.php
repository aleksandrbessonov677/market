<?php

use yii\db\Migration;

/**
 * Class m230509_123150_product
 */
class m230509_123150_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'latin_name' => $this->string(),
            'condition' => $this->json()->defaultValue(null),
            'price' => $this->float(),
            'discount' => $this->integer(),
            'preview' => $this->string(),
            'category_id' => $this->integer(),
            'shop_id' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('product');
    }
}
