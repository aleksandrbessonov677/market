<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;

class FlashMessage extends Widget
{
    const SUCCESS = 'success';
    const INFO = 'info';
    const ERROR = 'error';
    const WARNING = 'warning';

    const HEADER = [
        self::SUCCESS => 'Уведомление!',
        self::INFO => 'Информация!',
        self::ERROR => 'Ошибка!',
        self::WARNING => 'Предупреждение!',
    ];

    /**
     * Создание флеш сообщения
     * Пример создания сообщения: FlashMessage::createMessage(FlashMessage::SUCCESS, 'Ваше сообщение');
     * @param string $type - указываеться тип сообщения
     * @param string $message - указываеться сообщение
     */
    public static function createMessage(string $type, string $message)
    {
        Yii::$app->session->setFlash(uniqid($type . '_'), $message);
    }

    public function run()
    {
        $alerts = '';
        foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
            $realKey = explode('_', $key)[0];
            if (array_key_exists($realKey, self::HEADER)) {
                $header = self::HEADER[$realKey] ;
                $alerts .= "await Swal.fire('$header','$message','$realKey');";
            }
        }

        if ($alerts) {
            $this->view->registerJs(<<<JS
                (async () => { $alerts })();
            JS);
        }
    }
}