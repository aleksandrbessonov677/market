<?php

require dirname(__DIR__) . '/vendor/autoload.php';

$repository = Dotenv\Repository\RepositoryBuilder::createWithNoAdapters()
    ->addAdapter(Dotenv\Repository\Adapter\EnvConstAdapter::class)
    ->addWriter(Dotenv\Repository\Adapter\PutenvAdapter::class)
    ->immutable()
    ->make();
$dotenv = Dotenv\Dotenv::create($repository, dirname(__DIR__));
try {
    $dotenv->load();
} catch (\Throwable $throwable) {
}

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', (bool) $_ENV['YII_DEBUG']);
defined('YII_ENV') or define('YII_ENV', $_ENV['YII_ENV']);

require dirname(__DIR__) . '/vendor/yiisoft/yii2/Yii.php';