<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

class MessageHistorySearch extends MessageHistory
{
    public function rules()
    {
        return [
            [['id', 'client_id', 'created_at'], 'integer'],
            [['message'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MessageHistory::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['ilike', 'message', $this->message]);

        return $dataProvider;
    }
}
