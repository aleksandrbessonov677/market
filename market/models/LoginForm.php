<?php

namespace app\models;

use Yii;
use yii\base\Exception;
use yii\base\Model;

class LoginForm extends Model
{
    public $email;
    public $password;

    public bool $rememberMe = true;

    private $_user = false;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['email', 'password'], 'trim'],
            [['email', 'password'], 'required'],
            ['email', 'email'],
            ['rememberMe', 'boolean'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     * @return void
     */
    public function validatePassword($attribute, $params): void
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Почта или пароль указаны не верно!');
            }
        }
    }

    /**
     * @return string[]
     */
    public function attributeLabels(): array
    {
        return [
            'email' => 'E-mail',
            'password' => 'Пароль',
            'rememberMe' => 'Запомнить меня'
        ];
    }

    /**
     * @return bool
     */
    public function login(): bool
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
        }

        return false;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->email);
        }

        return $this->_user;
    }
}