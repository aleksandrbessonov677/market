<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Product
 *
 * @property integer $id
 * @property string $title
 * @property string $latin_name
 * @property array $condition
 * @property float $rating
 * @property string $description
 * @property float $price
 * @property integer $discount
 * @property string $preview
 * @property integer $category_id
 * @property integer $shop_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Category $category
 * @property Shop $shop
 */
class Product extends ActiveRecord
{
    public function behaviors(): array
    {
        return array_merge(parent::behaviors(), [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
        ]);
    }
    public static function tableName(): string
    {
        return 'product';
    }

    public function rules(): array
    {
        return [
            [['title', 'price', 'category_id', 'shop_id'], 'required'],
            [['title', 'latin_name', 'condition', 'description', 'preview'], 'string'],
            ['price', 'number', 'numberPattern' => '/^\d+(.\d{1,2})?$/'],
            [['discount', 'category_id', 'shop_id', 'created_at', 'updated_at'], 'integer']
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'latinName' => 'Латинское название',
            'condition' => 'Кондиция',
            'price' => 'Цена',
            'category_id' => 'Категорий',
            'shop_id' => 'Магазин',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
        ];
    }

    /** Relations */

    /** @return ActiveQuery */
    public function getCategory(): ActiveQuery
    {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }

    /** @return ActiveQuery */
    public function getShop(): ActiveQuery
    {
        return $this->hasOne(Shop::class, ['id' => 'shop_id']);
    }
}