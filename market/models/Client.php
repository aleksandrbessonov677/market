<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Client
 *
 * @property integer $id
 * @property integer $chat_id
 * @property string $username
 * @property string $first_name
 * @property string $last_name
 * @property string $photo
 * @property integer $created_at
 * @property integer $updated_at
 */
class Client extends ActiveRecord
{
    public function behaviors(): array
    {
        return array_merge(parent::behaviors(), [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
        ]);
    }

    public static function tableName(): string
    {
        return 'client';
    }

    public function rules(): array
    {
        return [
            [['chat_id', 'username', 'first_name', 'last_name', 'photo'], 'safe']
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'chat_id' => 'Chat_id',
            'username' => 'Username',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'photo' => 'Фото',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления'
        ];
    }
}