<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Shop
 *
 * @property integer $id
 * @property string $title
 * @property float $rating
 * @property string $address
 * @property string $site
 * @property string $email
 * @property string $phone
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 */
class Shop extends ActiveRecord
{
    public function behaviors(): array
    {
        return array_merge(parent::behaviors(), [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
        ]);
    }
    public static function tableName(): string
    {
        return 'shop';
    }

    public function rules(): array
    {
        return [
            ['title', 'required'],
            [['title', 'address', 'site', 'email', 'phone'], 'string'],
            ['email', 'email'],
            ['rating', 'number', 'numberPattern' => '/^\d+(.\d{1,2})?$/'],
            [['user_id', 'created_at', 'updated_at'], 'integer']
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'rating' => 'Рейтинг',
            'address' => 'Адрес',
            'site' => 'Сайт',
            'email' => 'E-mail',
            'phone' => 'Телефон',
            'user_id' => 'Ответственный',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения'
        ];
    }

    public static function getListItems(): array
    {
        return ArrayHelper::map(self::find()->select(['id', 'name'])->asArray()->all(), 'id', 'name');
    }

    /** Relations */

    /** @return ActiveQuery */
    public function getUser(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}