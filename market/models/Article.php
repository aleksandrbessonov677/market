<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Article
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property string $preview
 * @property integer $view_count
 * @property integer $article_category_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ArticleCategory $articleCategory
 */
class Article extends ActiveRecord
{
    public function behaviors(): array
    {
        return array_merge(parent::behaviors(), [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
        ]);
    }
    public static function tableName(): string
    {
        return 'article';
    }

    public function formName(): string
    {
        return '';
    }

    public function rules(): array
    {
        return [
            [['title', 'text', 'article_category_id'], 'required'],
            [['title', 'text', 'preview'], 'string'],
            [['article_category_id', 'view_count', 'created_at', 'updated_at'], 'integer']
        ];
    }

    /** Relations */

    /** @return ActiveQuery */
    public function getCategory(): ActiveQuery
    {
        return $this->hasOne(ArticleCategory::class, ['id' => 'article_category_id']);
    }
}