<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Category
 *
 * @property integer $id
 * @property string $title
 * @property integer $created_at
 * @property integer $updated_at
 */
class Category extends ActiveRecord
{
    public function behaviors(): array
    {
        return array_merge(parent::behaviors(), [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
        ]);
    }
    public static function tableName(): string
    {
        return 'category';
    }

    public function rules(): array
    {
        return [
            ['title', 'required'],
            ['title', 'string'],
            [['created_at', 'updated_at'], 'integer']
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения'
        ];
    }

    public static function getListItems(): array
    {
        return ArrayHelper::map(self::find()->select(['id', 'name'])->asArray()->all(), 'id', 'name');
    }
}