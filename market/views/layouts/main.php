<?php

use app\assets\LandingAsset;
use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var string $content */

LandingAsset::register($this);

$this->registerCsrfMetaTags();
$this->registerMetaTag(['charset' => Yii::$app->charset], 'charset');
$this->registerMetaTag(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1, shrink-to-fit=no']);
$this->registerMetaTag(['name' => 'description', 'content' => $this->params['meta_description'] ?? '']);
$this->registerMetaTag(['name' => 'keywords', 'content' => $this->params['meta_keywords'] ?? '']);
$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/x-icon', 'href' => Yii::getAlias('@web/favicon.ico')]);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,600&display=swap" rel="stylesheet">
    <link rel="preload" href="<?= Yii::getAlias('@web/landing/fonts/icofont.ttf') ?>" as="font">
    <link rel="preload" href="<?= Yii::getAlias('@web/landing/fonts/icofont.woff') ?>" as="font">
</head>
<body>
<?php $this->beginBody() ?>

<?= $content ?>

<!-- FOOTER SECTION -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h5>DOOB</h5>
                <h3>CREATIVITY ABOVE</h3>
                <ul class="contact-nav">
                    <li><a data-scroll href="#home">Главная</a></li>
                    <li><a data-scroll href="#about-us">О нас</a></li>
                    <li><a data-scroll href="#portfolio">Портфолио</a></li>
                    <li><a data-scroll href="#blog">Блог</a></li>
                    <li><a data-scroll href="#contact-us">Контакты</a></li>
                </ul>
                <h6>&copy; My Company <?= date('Y') ?></h6>
                <ul class="social">
                    <li><a href="#"><i class="icofont-facebook"></i></a></li>
                    <li><a href="#"><i class="icofont-twitter"></i></a></li>
                    <li><a href="#"><i class="icofont-dribbble"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
