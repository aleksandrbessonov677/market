<?php

/** @var yii\web\View $this */
/** @var string $content */


use app\assets\SneatAsset;
use yii\helpers\Html;

SneatAsset::register($this);

$this->registerCsrfMetaTags();
$this->registerMetaTag(['charset' => Yii::$app->charset], 'charset');
$this->registerMetaTag(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1, shrink-to-fit=no']);
$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/x-icon', 'href' => Yii::getAlias('@web/favicon.ico')]);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="light-style customizer-hide">
<head>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
        href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
        rel="stylesheet"
    />

    <link rel="stylesheet" href="<?= Yii::getAlias('@web/sneat/vendor/css/pages/page-misc.css') ?>">

    <script src="<?= Yii::getAlias('@web/sneat/vendor/js/helpers.js') ?>"></script>
    <script src="<?= Yii::getAlias('@web/sneat/js/config.js') ?>"></script>
</head>
<body>
<?php $this->beginBody() ?>

<div class="container-xxl container-p-y">
    <?= $content ?>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
