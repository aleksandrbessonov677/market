<?php

use app\models\LoginForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var LoginForm $model
 */

$this->title = 'Аутентификация';
?>

<div class="card">
    <div class="card-body">
        <h4 class="text-center mb-2">Добро пожаловать!</h4>
        <p class="mb-2">Пожалуйста, войдите в свою учетную запись</p>

        <?php $form = ActiveForm::begin([
            'id' => 'formAuthentication',
            'class' => 'mb-3',
        ]) ?>

        <div class="mb-3">
            <?= $form->field($model, 'email')
                ->textInput(['type' => 'email', 'class' => 'form-control', 'placeholder' => 'Введите ваш e-mail'])
                ->label('E-mail', ['class' => 'form-label']) ?>
        </div>

        <div class="mb-3 form-password-toggle">
            <div class="d-flex justify-content-between">
                <label class="form-label" for="password">Пароль</label>
                <a href="#">
                    <small>Забыли пароль?</small>
                </a>
            </div>
            <?php
            $template = '
                <div class="input-group input-group-merge">
                    {input}
                    <span class="input-group-text cursor-pointer"><i class="bx bx-hide"></i></span>
                </div>
                {hint} {error}';

            echo $form->field($model, 'password', ['template' => $template])
                ->passwordInput(['id' => 'password', 'class' => 'form-control', 'placeholder' => 'Введите пароль'])
                ->label(false) ?>
        </div>

        <div class="mb-3">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" id="remember-me" />
                <label class="form-check-label" for="remember-me"> Запомнить меня </label>
            </div>
        </div>

        <div class="mb-3">
            <?= Html::submitButton('Войти', ['class' => 'btn btn-primary d-grid w-100']) ?>
        </div>

        <?php ActiveForm::end() ?>

    </div>
</div>
