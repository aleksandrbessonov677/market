<?php

/** @var yii\web\View $this */

use yii\helpers\Url;

$this->title = 'Page Not Found';
?>

<div class="misc-wrapper">
    <h2 class="mb-2 mx-2">Page Not Found :(</h2>
    <p class="mb-4 mx-2">Oops! 😖 The requested URL was not found on this server.</p>
    <a href="<?= Url::to(Yii::$app->request->referrer ?? '/') ?>" class="btn btn-primary">Вернуться назад</a>
    <div class="mt-3">
        <img src="<?= Yii::getAlias('@web/sneat/img/illustrations/page-misc-error-light.png') ?>"
             alt="page-misc-error-light" width="500" class="img-fluid" data-app-dark-img="illustrations/page-misc-error-dark.png"
             data-app-light-img="illustrations/page-misc-error-light.png"
        >
    </div>
</div>
