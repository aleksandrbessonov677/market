<?php

namespace app\components\base;

use yii\helpers\Html;

class GridView extends \yii\grid\GridView
{
    public function init()
    {
        /** Кастомизация pager */
        $this->pager = [
            'options' => ['class' => 'pagination justify-content-center mt-4'],
            'prevPageLabel' => Html::tag('i', '', ['class' => 'tf-icon bx bx-chevrons-left']),
            'nextPageLabel' => Html::tag('i', '', ['class' => 'tf-icon bx bx-chevrons-right']),
            'linkContainerOptions' => ['class' => 'page-item'],
            'linkOptions' => ['class' => 'page-link'],
            'activePageCssClass' => 'active'
        ];

        /** Кстомизация layout */
        $this->layout = "{items}{pager}{summary}";

        /** Кстомизация summary */
        $this->summaryOptions = ['class' => 'summary m-4'];

        parent::init();
    }
}