<?php

use yii\helpers\Url;

$this->title = 'Настройки';
?>

<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-pills flex-column flex-md-row mb-3">
            <li class="nav-item">
                <a class="nav-link" href="<?= Url::to(['/admin/setting/index']) ?>"><i class="bx bxs-cog me-1"></i> Общие</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= Url::to(['/admin/setting/telegram']) ?>"><i class="bx bxl-telegram me-1"></i> Telegram Bot</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= Url::to(['/admin/setting/upload']) ?>"><i class="bx bxs-download me-1"></i> Выгрузка</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="<?= Url::to(['/admin/setting/php']) ?>"><i class="bx bxl-php me-1"></i> PHP Config</a>
            </li>
        </ul>
    </div>

    <div class="card">
        <h5 class="card-header">Php config</h5>
        <div class="card-body">
            <?= phpinfo() ?>
        </div>
    </div>
</div>