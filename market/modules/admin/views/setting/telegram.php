<?php

use app\models\TelegramSetting;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var TelegramSetting $model
 */

$this->title = 'Настройки';
?>

<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-pills flex-column flex-md-row mb-3">
            <li class="nav-item">
                <a class="nav-link" href="<?= Url::to(['/admin/setting/index']) ?>"><i class="bx bxs-cog me-1"></i> Общие</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="<?= Url::to(['/admin/setting/telegram']) ?>"><i class="bx bxl-telegram me-1"></i> Telegram Bot</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= Url::to(['/admin/setting/upload']) ?>"><i class="bx bxs-download me-1"></i> Выгрузка</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= Url::to(['/admin/setting/php']) ?>"><i class="bx bxl-php me-1"></i> PHP Config</a>
            </li>
        </ul>
    </div>

    <div class="card mb-4">
        <div class="card-header d-flex justify-content-between align-items-center">
            <h5 class="mb-0"><i class="bx bxl-telegram me-1"></i> Telegram Bot</h5>
            <small class="text-muted float-end">Настройки</small>
        </div>
        <div class="card-body">
            <?php $form = ActiveForm::begin(['options' => ['autocomplete' => 'off']]) ?>

            <div class="mb-3">
                <?= $form->field($model, 'name')->input('text',
                    ['class' => 'form-control', 'placeholder' => 'Введите имя бота']) ?>
            </div>

            <div class="mb-3">
                <?= $form->field($model, 'about')->input('text',
                    ['class' => 'form-control', 'placeholder' => 'Введите краткое описание']) ?>
            </div>

            <div class="mb-3">
                <?= $form->field($model, 'desc')
                    ->textarea(['class' => 'form-control', 'style' => 'min-height: 120px;',
                        'placeholder' => 'Введите приветсвенное сообщение']) ?>
            </div>

            <div class="mb-3">
                <?= $form->field($model, 'commands')
                    ->textarea(['class' => 'form-control', 'style' => 'min-height: 100px;',
                        'placeholder' => 'Введите команды, шаблон:
команда1 - описание
команда2 - описание']) ?>
            </div>

            <?= Html::submitButton('Сохранить изменения', ['class' => 'btn btn-primary me-2']) ?>

            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>
