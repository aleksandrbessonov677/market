<?php

use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Client $model */

$this->title = 'Клиент: ' . $model->id;
YiiAsset::register($this);
?>
<div class="client-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'chatId',
            'username',
            'firstName',
            'lastName',
            'created_at:datetime',
            'updated_at:datetime'
        ]
    ]) ?>

</div>
