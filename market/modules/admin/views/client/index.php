<?php

use app\components\base\GridView;
use app\models\Client;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var app\models\ClientSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Клиенты';
?>

<div class="card">
    <h5 class="card-header">
        <?= Html::encode($this->title) ?>
    </h5>
    <div class="table-responsive text-nowrap">
        <?php Pjax::begin() ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                'chatId',
                'username',
                'firstName',
                'lastName',
                'created_at:datetime',
                'updated_at:datetime',
                [
                    'class' => ActionColumn::className(),
                    'header'=>'Действия',
                    'template' => '{view}',
                    'urlCreator' => function ($action, Client $model, $key, $index, $column) {
                        return Url::toRoute([$action, 'id' => $model->id]);
                    }
                ],
            ]
        ]) ?>

        <?php Pjax::end() ?>
    </div>
</div>
