<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\User $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="card mb-4">
    <h5 class="card-header">Редактирование профиля</h5>
    <!-- Account -->
    <div class="card-body">
        <div class="d-flex align-items-start align-items-sm-center gap-4">
            <img src="<?= Yii::getAlias('@web/sneat/img/avatars/default.png') ?>" alt="user-avatar" class="d-block rounded" height="100" width="100" id="uploadedAvatar">
            <div class="button-wrapper">
                <label for="upload" class="btn btn-primary me-2 mb-4" tabindex="0">
                    <span class="d-none d-sm-block">Загрузить новое фото</span>
                    <i class="bx bx-upload d-block d-sm-none"></i>
                    <input type="file" id="upload" class="account-file-input" hidden="" accept="image/png, image/jpeg">
                </label>
                <button type="button" class="btn btn-outline-secondary account-image-reset mb-4">
                    <i class="bx bx-reset d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">Удалить</span>
                </button>

                <p class="text-muted mb-0">Allowed JPG, GIF or PNG. Max size of 800K</p>
            </div>
        </div>
    </div>
    <hr class="my-0">
    <div class="card-body">
        <?php $form = ActiveForm::begin(['options' => ['autocomplete' => 'off']]); ?>

        <div class="row">
            <div class="mb-3 col-md-6">
                <?= $form->field($model, 'firstName')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="mb-3 col-md-6">
                <?= $form->field($model, 'lastName')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="mb-3 col-md-6">
                <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="mb-3 col-md-6">
                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="mb-3 col-md-6">
                <?= $form->field($model, 'password')->passwordInput(['maxlength' => true, 'disabled' => true]) ?>
            </div>

            <div class="mb-3 col-md-6">
                <?= $form->field($model, 'role')->textInput(['maxlength' => true, 'disabled' => true]) ?>
            </div>

            <div class="mb-3 col-md-6">
                <?= $form->field($model, 'authKey')->textInput(['maxlength' => true, 'disabled' => true]) ?>
            </div>

            <div class="mb-3 col-md-6">
                <?= $form->field($model, 'accessToken')->textInput(['maxlength' => true, 'disabled' => true] ) ?>
            </div>
        </div>

        <div class="mt-2">
            <?= Html::submitButton('Сохранить изменения', ['class' => 'btn btn-primary me-2']) ?>
            <?= Html::a('Назад', Url::to(Yii::$app->request->referrer ?? '/admin'), ['class' => 'btn btn-outline-secondary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
    <!-- /Account -->
</div>
