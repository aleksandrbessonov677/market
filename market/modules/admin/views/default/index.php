<?php

/**
 * @var \yii\web\View $this
 * @var array $top
 */

use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;

$this->title = 'Аналитика'
?>

<div class="card">
    <div class="d-flex align-items-end row">
        <div class="col-sm-7">
            <div class="card-body">
                <h5 class="card-title text-primary">Congratulations John! 🎉</h5>
                <p class="mb-4">
                    You have done <span class="fw-bold">72%</span> more sales today. Check your new badge in
                    your profile.
                </p>

                <a href="javascript:;" class="btn btn-sm btn-outline-primary">View Badges</a>
            </div>
        </div>
        <div class="col-sm-5 text-center text-sm-left">
            <div class="card-body pb-0 px-0 px-md-4">
                <img src="<?= Yii::getAlias('@web/sneat/img/illustrations/man-with-laptop-light.png') ?>" height="140" alt="View Badge User" data-app-dark-img="illustrations/man-with-laptop-dark.png" data-app-light-img="illustrations/man-with-laptop-light.png">
            </div>
        </div>
    </div>
</div>

<div class="row mt-4">
    <div class="col-12 col-lg-8 order-2 order-md-3 order-lg-2 mb-4">
        <div class="card">
            <div class="row row-bordered g-0 pt-2">
                <h5 class="card-header m-0 me-2 pb-3">Новые клиенты</h5>
                <?php
//                Highcharts::widget([
//                    'scripts' => [
//                        'modules/exporting',
//                        'themes/grid-light',
//                    ],
//                    'options' => [
//                        'title' => [
//                            'text' => 'Новые клиенты',
//                        ],
//                        'xAxis' => [
//                            'categories' => ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
//                        ],
//                        'yAxis' => [
//                            'title' => ['text' => 'Количесво']
//                        ],
//                        'labels' => [
//                            'items' => [
//                                [
//                                    'html' => 'Общее количесвто',
//                                    'style' => [
//                                        'left' => '50px',
//                                        'top' => '18px',
//                                        'color' => new JsExpression('(Highcharts.theme && Highcharts.theme.textColor) || "black"'),
//                                    ],
//                                ],
//                            ],
//                        ],
//                        'series' => [
//                            [
//                                'type' => 'column',
//                                'name' => 'Jane',
//                                'data' => [3, 2, 1, 3, 4, 2, 1, 3, 4],
//                            ],
//                            [
//                                'type' => 'column',
//                                'name' => 'John',
//                                'data' => [2, 3, 5, 7, 6],
//                            ],
//                            [
//                                'type' => 'column',
//                                'name' => 'Joe',
//                                'data' => [4, 3, 3, 9, 0],
//                            ],
//                            [
//                                'type' => 'spline',
//                                'name' => 'Average',
//                                'data' => [3, 2.67, 3, 6.33, 3.33],
//                                'marker' => [
//                                    'lineWidth' => 2,
//                                    'lineColor' => new JsExpression('Highcharts.getOptions().colors[3]'),
//                                    'fillColor' => 'white',
//                                ],
//                            ],
//                            [
//                                'type' => 'pie',
//                                'name' => 'Total consumption',
//                                'data' => [
//                                    [
//                                        'name' => 'Jane',
//                                        'y' => 13,
//                                        'color' => new JsExpression('Highcharts.getOptions().colors[0]'), // Jane's color
//                                    ],
//                                    [
//                                        'name' => 'John',
//                                        'y' => 23,
//                                        'color' => new JsExpression('Highcharts.getOptions().colors[1]'), // John's color
//                                    ],
//                                    [
//                                        'name' => 'Joe',
//                                        'y' => 19,
//                                        'color' => new JsExpression('Highcharts.getOptions().colors[2]'), // Joe's color
//                                    ],
//                                ],
//                                'center' => [100, 80],
//                                'size' => 100,
//                                'showInLegend' => false,
//                                'dataLabels' => [
//                                    'enabled' => false,
//                                ],
//                            ],
//                        ],
//                    ]
//                ]);
                ?>
            </div>
        </div>
    </div>

    <div class="col-12 col-lg-4 order-2 order-md-3 order-lg-2 mb-4">
        <div class="card">
            <div class="row row-bordered g-0">
                <h5 class="card-header m-0 me-2 pb-3">Топ запросов</h5>

                <div class="card-body">
                    <ul class="p-0 m-0">
                        <?php foreach ($top as $item): ?>
                        <li class="d-flex pb-1 w-100 flex-wrap align-items-center justify-content-between gap-2">
                            <h6 class="m-0 p-0"><?= $item['message'] ?></h6>
                            <h6 class="m-0 p-0"><?= $item['message_count'] ?></h6>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="col-12 col-lg-8 order-2 order-md-3 order-lg-2 mb-4">
        <div class="card">
            <div class="row row-bordered g-0">
                <h5 class="card-header m-0 me-2 pb-3">Количесво запросов</h5>
                <?php
//                Highcharts::widget([
//                    'options' => [
//                        'title' => ['text' => 'Fruit Consumption'],
//                        'xAxis' => [
//                            'categories' => ['Apples', 'Bananas', 'Oranges']
//                        ],
//                        'yAxis' => [
//                            'title' => ['text' => 'Fruit eaten']
//                        ],
//                        'series' => [
//                            ['name' => 'Jane', 'data' => [1, 0, 4]],
//                            ['name' => 'John', 'data' => [5, 7, 3]]
//                        ]
//                    ]
//                ]);
//                ?>
            </div>
        </div>
    </div>

</div>

<?php
  $this->registerCss(<<<CSS
        .highcharts-credits {
            display: none !important;
        }
    CSS);
?>
