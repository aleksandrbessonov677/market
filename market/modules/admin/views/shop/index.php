<?php

use app\components\base\GridView;
use app\models\Shop;
use app\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var app\models\ShopSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Магазины';
?>

<div class="card">
    <h5 class="card-header">
        <?= Html::encode($this->title) ?>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success', 'style' => 'float: right;']) ?>
    </h5>

    <div class="table-responsive text-nowrap">
        <?php Pjax::begin() ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table'],
            'columns' => [
                'id',
                'name',
                'rating',
                'user_id' => [
                    'attribute'=>'user_id',
                    'label' => 'Отвественный',
                    'format' => 'raw',
                    'filter' => User::getListItems(),
                    'value' => function (Shop $model) {
                        return $model->user->getFullName();
                    }
                ],
                'created_at:datetime',
                'updated_at:datetime',
                [
                    'class' => ActionColumn::className(),
                    'header'=>'Действия',
                    'urlCreator' => function ($action, Shop $model, $key, $index, $column) {
                        return Url::toRoute([$action, 'id' => $model->id]);
                    }
                ],
            ],
        ]) ?>

        <?php Pjax::end() ?>
    </div>
</div>
