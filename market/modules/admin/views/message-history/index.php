<?php

use app\components\base\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var app\models\MessageHistorySearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'История запросов';
?>

<div class="card">
    <h5 class="card-header">
        <?= Html::encode($this->title) ?>
    </h5>
    <div class="table-responsive text-nowrap">
        <?php Pjax::begin() ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                'message:ntext',
                'client_id' => [
                    'attribute' => 'client_id',
                    'format' => 'html',
                    'value' => function ($model) {
                        if ($client = $model->client) {
                            return Html::a($client->username, Url::to(['/admin/client/view', 'id' => $client->id]), ['target' => '_blank']);
                        } else {
                            return 'Пользователь не найден!';
                        }
                    }
                ],
                'created_at:datetime',
            ]
        ]) ?>

        <?php Pjax::end() ?>
    </div>
</div>
