<?php

use yii\helpers\Url;
?>

<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
    <div class="app-brand demo">
        <a href="<?= Url::to('/admin') ?>" class="app-brand-link">
            <h3 class="app-brand-text demo menu-text fw-bolder ms-2">Your CRM</h3>
        </a>

        <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
            <i class="bx bx-chevron-left bx-sm align-middle"></i>
        </a>
    </div>

    <div class="menu-inner-shadow"></div>

    <ul class="menu-inner py-1">
        <!-- Dashboard -->
        <li class="menu-item <?= Yii::$app->controller->id == 'default' ? 'active' : '' ?>">
            <a href="<?= Url::to('/admin') ?>" class="menu-link">
                <i class="menu-icon tf-icons bx bx-home-circle"></i>
                <div data-i18n="Analytics">Dashboard</div>
            </a>
        </li>

        <!-- Main -->
        <li class="menu-header small text-uppercase"><span class="menu-header-text">Основное</span></li>
        <!-- Markets -->
        <li class="menu-item <?= Yii::$app->controller->id == 'shop' ? 'active' : '' ?>">
            <a href="<?= Url::to('/admin/shop') ?>" class="menu-link">
                <i class="menu-icon tf-icons bx bx-collection"></i>
                <div>Магазины</div>
            </a>
        </li>

        <li class="menu-item <?= Yii::$app->controller->id == 'category' ? 'active' : '' ?>">
            <a href="<?= Url::to('/admin/category') ?>" class="menu-link">
                <i class="menu-icon tf-icons bx bx-collection"></i>
                <div>Категории товаров</div>
            </a>
        </li>

        <li class="menu-item <?= Yii::$app->controller->id == 'product' ? 'active' : '' ?>">
            <a href="<?= Url::to('/admin/product') ?>" class="menu-link">
                <i class="menu-icon tf-icons bx bx-collection"></i>
                <div>Товары</div>
            </a>
        </li>

        <!-- Forms & Tables -->
        <li class="menu-header small text-uppercase"><span class="menu-header-text">Администрирование</span></li>
        <!-- Settings -->
        <li class="menu-item <?= Yii::$app->controller->id == 'setting' ? 'active' : '' ?>">
            <a href="<?= Url::to('/admin/setting') ?>" class="menu-link">
                <i class="menu-icon tf-icons bx bx-cog"></i>
                <div>Настройки</div>
            </a>
        </li>
        <!-- Users -->
        <li class="menu-item <?= Yii::$app->controller->id == 'user' ? 'active' : '' ?>">
            <a href="<?= Url::to('/admin/user') ?>" class="menu-link">
                <i class="menu-icon tf-icons bx bx-user"></i>
                <div>Пользователи</div>
            </a>
        </li>
        <!-- Clients -->
        <li class="menu-item <?= Yii::$app->controller->id == 'client' ? 'active' : '' ?>">
            <a href="<?= Url::to('/admin/client') ?>" class="menu-link">
                <i class="menu-icon tf-icons bx bx-user-check"></i>
                <div>Клиенты</div>
            </a>
        </li>
        <!-- Message history -->
        <li class="menu-item <?= Yii::$app->controller->id == 'message-history' ? 'active' : '' ?>">
            <a href="<?= Url::to('/admin/message-history') ?>" class="menu-link">
                <i class="menu-icon tf-icons bx bx-message"></i>
                <div>История запросов</div>
            </a>
        </li>
    </ul>
</aside>
