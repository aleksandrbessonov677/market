<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\ProductSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Загрузка товаров';
?>

<div class="card">
    <h5 class="card-header">
        <?= Html::encode($this->title) ?>
    </h5>

    <div class="card-body">
        <?php ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
        <div class="mb-3">
            <label for="formFile" class="form-label">Файл со списком товаров</label>
            <div class="input-group">
                <input name="file" class="form-control" type="file" id="formFile" aria-describedby="button-addon">
                <button type="submit" class="btn btn-outline-primary" id="button-addon">Загрузить</button>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>
</div>

