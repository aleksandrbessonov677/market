<?php

use app\components\base\GridView;
use app\models\Category;
use app\models\Product;
use app\models\Shop;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var app\models\ProductSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Товары';
?>

<div class="card">
    <h5 class="card-header">
        <div class="d-flex justify-content-between">
            <?= Html::encode($this->title) ?>

            <div>
                <?= Html::a('Загрузить', ['upload'], ['class' => 'btn btn-primary mr-4']) ?>
                <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </h5>

    <div class="table-responsive text-nowrap">
        <?php Pjax::begin() ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table'],
            'columns' => [
                'id',
                'name',
                'latinName',
//                'condition',
                'price',
                'category_id' => [
                    'attribute' => 'category_id',
                    'filter' => Category::getListItems(),
                    'label' => 'Категория',
                    'value' => function (Product $model) {
                        return $model->category->name ?? 'Не назначена';
                    }
                ],
                'shop_id' => [
                    'attribute' => 'shop_id',
                    'filter' => Shop::getListItems(),
                    'label' => 'Магазин',
                    'value' => function (Product $model) {
                        return $model->shop->name ?? 'Не назначен';
                    }
                ],
//                'created_at:datetime',
//                'updated_at:datetime',
                [
                    'class' => ActionColumn::className(),
                    'header'=>'Действия',
                    'urlCreator' => function ($action, Product $model, $key, $index, $column) {
                        return Url::toRoute([$action, 'id' => $model->id]);
                    }
                ],
            ],
        ]) ?>

        <?php Pjax::end() ?>
    </div>
</div>
