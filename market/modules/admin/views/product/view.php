<?php

use app\models\Product;
use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Product $model */

$this->title = 'Товар: ' . $model->name;
YiiAsset::register($this);
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены что хотите удалить магазин?',
                'method' => 'post',
            ]
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'latinName',
            'condition',
            'price',
            'category_id' => [
                'label' => 'Категория',
                'value' => function (Product $model) {
                    return $model->category->name ?? 'Не определена';
                }
            ],
            'shop_id' => [
                'label' => 'Магазин',
                'value' => function (Product $model) {
                    return $model->shop->name ?? 'Не определен';
                }
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
