<?php

namespace app\modules\admin\controllers;

use app\models\MessageHistory;
use yii\web\Controller;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $topRequest = MessageHistory::find()->select(['message', 'COUNT(*) as message_count'])
            ->groupBy('message')->orderBy(['message_count' => SORT_DESC])->limit(30)->asArray()->all();

        return $this->render('index', ['top' => $topRequest]);
    }
}
