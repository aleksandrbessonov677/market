<?php

namespace app\modules\admin\controllers;

use app\models\TelegramSetting;
use app\widgets\FlashMessage;
use Yii;
use yii\web\Controller;

class SettingController extends Controller
{
    public function actionIndex(): string
    {
        return $this->render('index');
    }

    public function actionTelegram(): string
    {
        $settingModel = TelegramSetting::find()->one();

        if (is_null($settingModel)) {
            $settingModel = new TelegramSetting();
        }

        if ($post = Yii::$app->request->post()) {
            if ($settingModel->load($post) && $settingModel->save()) {
                FlashMessage::createMessage(FlashMessage::SUCCESS, 'Настройки бота были успешно сохранены!');
            }
        }

        return $this->render('telegram', ['model' => $settingModel]);
    }

    public function actionUpload(): string
    {
        return $this->render('upload');
    }

    public function actionPhp(): string
    {
        return $this->render('php');
    }
}