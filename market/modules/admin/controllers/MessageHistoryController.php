<?php

namespace app\modules\admin\controllers;

use app\models\MessageHistorySearch;
use yii\web\Controller;

class MessageHistoryController extends Controller
{
    public function actionIndex(): string
    {
        $searchModel = new MessageHistorySearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
