<?php

namespace app\modules\api\controllers;

use app\models\Shop;
use app\modules\api\rest\Controller;

class ShopController extends Controller
{
    public $modelClass = Shop::class;
}