<?php

namespace app\modules\api\controllers;

use app\models\Article;
use app\modules\api\rest\Controller;
use yii\web\Response;

class ArticleController extends Controller
{
    public $modelClass = Article::class;
}