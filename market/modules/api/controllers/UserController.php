<?php

namespace app\modules\api\controllers;

use app\models\User;
use app\modules\api\rest\Controller;

class UserController extends Controller
{
    public $modelClass = User::class;
}