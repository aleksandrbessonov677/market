<?php

namespace app\modules\api\controllers;

use app\models\ArticleCategory;
use app\modules\api\rest\Controller;

class ArticleCategoryController extends Controller
{
    public $modelClass = ArticleCategory::class;
}