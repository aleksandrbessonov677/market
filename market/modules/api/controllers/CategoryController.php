<?php

namespace app\modules\api\controllers;

use app\models\Category;
use app\modules\api\rest\Controller;

class CategoryController extends Controller
{
    public $modelClass = Category::class;
}