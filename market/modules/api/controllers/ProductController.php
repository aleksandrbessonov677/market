<?php

namespace app\modules\api\controllers;

use app\models\Product;
use app\modules\api\rest\Controller;

class ProductController extends Controller
{
    public $modelClass = Product::class;
}