<?php

namespace app\modules\api\controllers;

use app\models\LoginForm;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\Cors;
use yii\rest\Controller;
use app\modules\api\rest\RestApiHelper;
use Yii;
use yii\web\Cookie;

class AuthController extends Controller
{
    /**
     * {@inheritdoc}
     */
    protected function verbs(): array
    {
        return [
            'login' => ['POST'],
            'logout' => ['POST'],
        ];
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return Yii::$app->user->identity;
        }

        $model = new LoginForm();
        $requestParams = Yii::$app->request->bodyParams;
        if ($model->load($requestParams, '') && $model->login()) {
            $user = Yii::$app->user->identity;
            $cookie = new Cookie([
                'name' => 'authKey',
                'value' => $user->authKey,
                'expire' => time() + 86400,
                'httpOnly' => true
            ]);

            Yii::$app->getResponse()->getCookies()->add($cookie);
            return $user;
        }

        $model->password = '';
        return RestApiHelper::serializerErrors($model->errors);
    }

    public function actionLogout(): void
    {
        Yii::$app->user->logout();
        Yii::$app->response->cookies->remove('authKey');
    }
}