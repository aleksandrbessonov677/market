<?php

namespace app\modules\api\rest;

use yii\filters\auth\AuthMethod;

class HttpCookieAuth extends AuthMethod
{
    /**
     * @var string
     */
    public $cookie = 'authKey';

    /**
     * {@inheritdoc}
     */
    public function authenticate($user, $request, $response)
    {
        $cookie = $request->getCookies()->get($this->cookie);
        if ($cookie !== null) {
            $identity = $user->loginByAccessToken($cookie->value, get_class($this));
            if ($identity === null) {
                $this->challenge($response);
                $this->handleFailure($response);
            }
            return $identity;
        }

        return null;
    }
}