<?php

namespace app\modules\api\rest;

use yii\rest\ActiveController;

class Controller extends ActiveController
{
    use BehaviorsTrait;

    public $serializer = Serializer::class;

    /**
     * {@inheritdoc}
     */
    protected function verbs(): array
    {
        return [
            'index' => ['GET', 'POST'],
            'view' => ['GET'],
            'create' => ['PUT'],
            'update' => ['PATCH'],
            'delete' => ['DELETE'],
        ];
    }
}