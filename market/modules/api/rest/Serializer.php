<?php

namespace app\modules\api\rest;

class Serializer extends \yii\rest\Serializer
{
    /**
     * Переопределил добавления пагинации для апи - убрал
     * ссылки текущей, предыдущей и следующей страницы из хедера
     *
     * @param $pagination
     * @return void
     */
    protected function addPaginationHeaders($pagination)
    {
        $this->response->getHeaders()
            ->set($this->totalCountHeader, $pagination->totalCount)
            ->set($this->pageCountHeader, $pagination->getPageCount())
            ->set($this->currentPageHeader, $pagination->getPage() + 1)
            ->set($this->perPageHeader, $pagination->pageSize);
    }
}