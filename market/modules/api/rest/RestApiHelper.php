<?php

namespace app\modules\api\rest;

use Yii;
use yii\data\DataProviderInterface;

class RestApiHelper
{
    /**
     * Сериализует ошибки моделей для однотипного вывода
     *
     * @param array $errors
     * @return array
     */
    public static function serializerErrors(array $errors): array
    {
        Yii::$app->response->setStatusCode(422, 'Data validation failed');

        $result = [];
        foreach ($errors as $field => $messages) {
            foreach ($messages as $message) {
                $result[] = ['field' => $field, 'message' => $message];
            }
        }

        return $result;
    }

    /**
     * Устанваливает хедеры для пагинации из датапровайдера
     * Внимание: делает вызов getModels для получения пагинации
     *
     * @param DataProviderInterface $dataProvider
     * @return void
     */
    public static function setPaginationHeaders(DataProviderInterface $dataProvider): void
    {
        $dataProvider->getModels();
        $headers = Yii::$app->response->getHeaders();
        $headers->set('X-Pagination-Total-Count', $dataProvider->totalCount);
        $headers->set('X-Pagination-Page-Count', $dataProvider->pagination->getPageCount());
        $headers->set('X-Pagination-Current-Page', $dataProvider->pagination->page ?: 1);
        $headers->set('X-Pagination-Per-Page', $dataProvider->pagination->pageSize);
    }
}