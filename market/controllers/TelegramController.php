<?php

namespace app\controllers;

use aki\telegram\base\Input;
use aki\telegram\Telegram;
use app\models\Client;
use app\models\MessageHistory;
use app\models\Product;
use Yii;
use yii\base\InvalidRouteException;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class TelegramController extends Controller
{
    public $enableCsrfValidation = false;

    /**
     * @throws InvalidRouteException|\GuzzleHttp\Exception\GuzzleException
     */
    public function actionIndex()
    {
        try {
            /** @var Telegram $telegram */
            $telegram = Yii::$app->telegram;

            /** @var Input $input */
            $input = $telegram->input;

            $this->input($input);
        } catch (\Exception $exception) {
            Yii::error($exception);
        }
    }

    public function input($input)
    {
        $query = $input->getCallback_query();
        $message = $input->message->text ?? null;

        if ($message) {
            $chat = $input->message->chat;

            $history = new MessageHistory();
            $history->message = $message;
            $history->client_id = $chat->id ?? null;
            $history->created_at = time();
            $history->save(false);

            switch ($message) {
                case '/start';
                    $client = Client::findOne(['chatId' => $chat->id]);

                    if (!$client) {
                        $client = new Client();
                    }

                    $client->chatId = $chat->id;
                    $client->username = $chat->username ?? '';
                    $client->firstName = $chat->firstName ?? '';
                    $client->lastName = $chat->lastName ?? '';
                    $client->photo = $chat->photo ?? '';
                    $client->save(false);

                    $response = 'Просто напишите название растения! Русское, латинское, вид - чем точнее, тем лучше результат.';
                    Yii::$app->telegram->sendMessage([
                        'chat_id' => $chat->id,
                        'text' => $response
                    ]);
                    break;

                case '/ref';
                    $response =  <<<HTML
                Как разместить ассортимент вашего питомника в боте? Пишите https://t.me/findyourplantinfo
                HTML;
                    Yii::$app->telegram->sendMessage([
                        'chat_id' => $chat->id,
                        'text' => $response
                    ]);
                    break;

                case '/support';
                    $response = <<<HTML
                Вопросы, пожелания, советы и любой другой вид взаимодействия - https://t.me/findyourplantinfo
                HTML;
                    Yii::$app->telegram->sendMessage([
                        'chat_id' => $chat->id,
                        'text' => $response
                    ]);
                    break;

                case '/condition';
                    $response = <<<HTML
                Размеры растений:
                15-20, 100-125, 175-200 и т.п.  -  высота растений в см;
                100/+, 80/+ и т.п. - высота растения выше (или равна) указанной величины в см;
                10/12, 20/25 и т.п. - обхват ствола (длина окружности) у дереьев на высоте 1 м;
                150-175*100-125 - первый размер - высота растения, второй его ширина в см (дает представление об объеме кроны у растений, например у горных сосен).

                Корневая система:
                Сo 2 l, C12, VP 2  -   и т.п. - обозначение контейнера и его объем в литрах;
                p9, p12 и т.п. - обозначение контейнера и его диаметр в см;
                rb - земляной ком обернутый мешковиной и затянутый мет. сеткой (только крупные);
                Pl.bag - геотекстильная сумка;
                3xv, 4xv и т.д. - кол-во пересадок у данного растения (3xv - три пересадки и т.д.);

                Прочее:
                Extra - растения высшего качества
                Sol - крупное красивое растение для одиночной посадки.
                St. - обозначения штамба (ствола) у растений (напр., St.80 - длина ствола от земли до кроны 80 см)
                2/3 br. - количество ветвей (у кустов) или стволов (у деревьев)
                Spalier, Ball, Spiral, Bonsai - различные стриженые формы растений
                HTML;
                    Yii::$app->telegram->sendMessage([
                        'chat_id' => $chat->id,
                        'text' => $response
                    ]);
                    break;

                default:
                    try {
                        $query = Product::find()->alias('p')->joinWith("shop s")->where(['@@', 'p.name', $message])->orWhere(['@@', 'p.latinName', $message])
                            ->orderBy(["p.price" => SORT_ASC, "s.rating" => SORT_DESC]);
                        $dataProvider = new ActiveDataProvider([
                            'query' => $query,
                            'pagination' => [
                                'pageSize' => 10,
                            ],
                        ]);

                        if ($dataProvider->totalCount > 0) {
                            $products = $dataProvider->getModels();

                            $productList = [];
                            /** @var Product $product */
                            foreach ($products as $key => $product) {
                                $shop = $product->shop;
                                $key ++;
                                $productList[] = "$key - $product->name, $product->latinName, $product->condition, $product->price руб., $shop->name, $shop->site, $shop->phone, $shop->address";
                            }

                            $response = implode(PHP_EOL, $productList);
                            $currentPage = $dataProvider->pagination->page === 0 ? 1 : $dataProvider->pagination->page;
                            $totalPage = $dataProvider->pagination->pageCount;

                            Yii::$app->telegram->sendMessage([
                                'chat_id' => $chat->id,
                                'text' => $response,
                                'reply_markup' => json_encode([
                                    'inline_keyboard'=>[
                                        [
                                            ['text' => "←", 'callback_data'=> "prompt:$message;page:0"],
                                            ['text' => "$currentPage / $totalPage", 'callback_data'=> "None"],
                                            ['text' => "→", 'callback_data'=> "prompt:$message;page:1"]
                                        ],
                                    ]
                                ])
                            ]);
                        } else {
                            $response = 'По вашему запросу ни чего не найдено';
                            Yii::$app->telegram->sendMessage([
                                'chat_id' => $chat->id,
                                'text' => $response
                            ]);
                        }
                    } catch (\Exception $exception) {
                        Yii::error($exception);
                    }
            }
        }

        if ($query) {
            $chatId = $query->message['chat']['id'];
            $messageId = $query->message['message_id'];

            $data = explode(';', $query->data);

            $commands = [];
            foreach ($data as $item) {
                $arr = explode(':', $item);
                $commands[$arr[0]] = $arr[1];
            }

            $message = $commands['prompt'];
            try {
                $query = Product::find()->alias('p')->joinWith("shop s")->where(['@@', 'p.name', $message])->orWhere(['@@', 'p.latinName', $message])
                    ->orderBy(["p.price" => SORT_ASC, "s.rating" => SORT_DESC]);
                $dataProvider = new ActiveDataProvider([
                    'query' => $query,
                    'pagination' => [
                        'pageSize' => 10,
                    ],
                ]);

                if ($dataProvider->totalCount > 0) {
                    $totalPage = (int) (($dataProvider->totalCount + $dataProvider->pagination->pageSize - 1) / $dataProvider->pagination->pageSize);

                    if ($currentPage = $commands['page']) {
                        if ($currentPage < 0) {
                            $currentPage = 0;
                        }

                        if ($currentPage > $totalPage) {
                            $currentPage = $totalPage;
                        }
                    }

                    $dataProvider->pagination->page = $currentPage;

                    $products = $dataProvider->getModels();

                    $productList = [];
                    /** @var Product $product */
                    foreach ($products as $key => $product) {
                        $shop = $product->shop;
                        $key ++;
                        $lastKey = $currentPage * 10;
                        $key = $lastKey + $key;
                        $productList[] = "$key - $product->name, $product->latinName, $product->condition, $product->price руб., $shop->name, $shop->site, $shop->phone, $shop->address";
                    }

                    $response = implode(PHP_EOL, $productList);

                    $prevPage = $currentPage - 1;
                    $currPage = $currentPage + 1;
                    $nextPage = $currentPage + 1;

                    Yii::$app->telegram->editMessageText([
                        'chat_id' => $chatId,
                        'message_id' => $messageId,
                        'text' => $response,
                        'reply_markup' => json_encode([
                            'inline_keyboard'=>[
                                [
                                    ['text' => "←", 'callback_data'=> "prompt:$message;page:$prevPage"],
                                    ['text' => "$currPage / $totalPage", 'callback_data'=> "None"],
                                    ['text' => "→", 'callback_data'=> "prompt:$message;page:$nextPage"]
                                ],
                            ]
                        ])
                    ]);

                } else {

                    $response = 'Произошла ошибка повторите позже!';
                    Yii::$app->telegram->sendMessage([
                        'chat_id' => $chatId,
                        'text' => $response
                    ]);
                }
            } catch (\Exception $exception) {
                Yii::error($exception);
            }
        }
    }
}