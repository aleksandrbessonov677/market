<?php

namespace app\assets;

use yii\web\AssetBundle;

class SneatAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'sneat/vendor/fonts/boxicons.css',
        'sneat/vendor/css/core.css',
        'sneat/vendor/css/theme-default.css',
        'sneat/vendor/libs/perfect-scrollbar/perfect-scrollbar.css',
        'sneat/css/site.css',
    ];

    public $js = [
        'sneat/js/sweetalert2.all.min.js',
        'sneat/vendor/libs/popper/popper.js',
        'sneat/vendor/js/bootstrap.js',
        'sneat/vendor/libs/perfect-scrollbar/perfect-scrollbar.js',
        'sneat/vendor/js/menu.js',
        'sneat/js/main.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];
}