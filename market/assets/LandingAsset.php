<?php

namespace app\assets;

use yii\web\AssetBundle;

class LandingAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'landing/css/bootstrap.min.css',
        'landing/css/icofont.min.css',
        'landing/css/slick.css',
        'landing/css/main.css',
    ];

    public $js = [
        'landing/js/bootstrap.min.js',
        'landing/js/smooth-scroll.min.js',
        'landing/js/slick.min.js',
        'landing/js/main.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];
}